<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class B2c_groups extends Model
{
	protected $table = 'b2c_groups';

    protected $primaryKey = 'id_group';

	public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        'id_group','name', 'describe','image','active','created_by',
    ];

    public static $validateID = [
        'id_user'=> 'required',
    ];

    public static $validateIDGroup = [
        'id_group' => 'required',
    ];

    public static $validateImage = [
        'id_group'  => 'required|min:20',
        'image'     => 'required|image'
    ];

    public static $validateEdit = [
        'id_group'  => 'required|min:20',
        'name'      => 'required',
    ];

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])
            ->translatedFormat('l, d F Y');
    }
}