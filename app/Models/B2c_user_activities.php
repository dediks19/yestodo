<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class B2c_user_activities extends Model
{
	protected $table = 'b2c_user_activities';

	protected $primaryKey = 'id_activity';

	protected $fillable = [
		'id_activity', 'fk_task', 'fk_user', 'fk_actitype'
	];

	public static $validateID = [
		'id_task' => 'required|min:20'
	];

	public $timestamps = false;
	
	public $incrementing = false;
}