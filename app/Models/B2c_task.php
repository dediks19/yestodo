<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class B2c_task extends Model
{
	protected $table = 'b2c_task';

	protected $primaryKey = 'id_task';

	protected $fillable = [
		'id_task', 'fk_group', 'title', 'describe', 'archived', 'created_by', 'deadline', 'fk_last_activity'
	];

	public static $validateGroupID = [
		'fk_group' 		=> 'required|min:20'
	];

	public static $validateTask = [
		'fk_group' 	=> 'required|min:20',
		'title' 	=> 'required',
	];

	public static $validateDetail = [
		'id_task' => 'required|min:20',
	];

	public static $validateMove = [
		'id_task' 	=> 'required|min:20',
		'id_user' 	=> 'required|min:20',
		'activity' 	=> 'required|max:2',
	];

	public static $validateTaskAndMember = [
		'id_user' => 'required|min:20',
		'id_task' => 'required|min:20'
	];

	public static $validateInput = [
		'id_task' 	=> 'required|min:20',
		'members' 		=> 'required'
	];

	public $timestamps = false;

	public $incrementing = false;

	public function getDeadlineAttribute()
	{
		return Carbon::parse($this->attributes['deadline'])
            ->translatedFormat('d-m-Y H:i');
	}
}