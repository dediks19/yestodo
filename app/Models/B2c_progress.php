<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class B2c_progress extends Model
{
	protected $table = 'b2c_progress';

	protected $primaryKey = 'id_progress';

	protected $fillable = [
		'id_progress', 'fk_user', 'fk_task_member', 'texts',
	];

	public static $validateCreate = [
		'id_user' => 'required|min:20',
		'id_task_member' => 'required|min:20',
	];

	public static $validateID = [
		'id_task_member'=>'required',
	];

	public $timestamps = false;

	public $incrementing = false;
}