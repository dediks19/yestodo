<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class B2c_comments extends Model
{
	protected $table = 'b2c_comments';

	protected $primaryKey = 'id_comment';

	protected $fillable = [
		'id_comment', 'fk_task', 'fk_user', 'comment', 'status_rmv',
	];

	public static $validateAddComment = [
		'id_user', 'id_task', 'comment',
	];

	public $timestamps = false;

	public $incrementing = false;
}