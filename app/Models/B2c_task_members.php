<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class B2c_task_members extends Model
{
	protected $table = 'b2c_task_members';

	protected $primaryKey = 'id_task_member';

	protected $fillable = [
		'id_task_member', 'fk_user', 'fk_task_member'
	];

	public static $validateTaskMember = [
		'fk_user'
	];

	public static $validateTask = [
		'id_task' => 'required|min:20',
	];

	public $timestamps = false;

	public $incrementing = false;
}