<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helper\Helper;


class Anggota_Sistem extends Model
{
	protected $table = 'anggota_sistem';

	protected $primaryKey = 'id_anggota';

	public static $validateID = [
		'id_user' => 'required|min:20'
	];

	public $timestamps = false;

	public $incrementing = false;

	public function getImageUrlAttribute() {
        return Helper::getProfilePath($this->foto_anggota);
    }
}