<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helper\Helper;
use Illuminate\Support\Carbon;


class Daily_Reports extends Model
{
	protected $table = 'daily_reports';

	protected $primaryKey = 'id_report';

	

	protected $fillable = [
		'id_report','message ','image','from','to','rmv_status'
	];

	public $timestamps = false;

	public $incrementing = false;

	public static $validateInput = [
		'id_user' => 'required|min:20',
	];

	public static $validateFK = [
		'id_user' 	=> 'required|min:20',
		'fk_user' 	=> 'required|min:20'
	];

	public static $validateID = [
		'id_user' 	=> 'required|min:20'
	];

	public function getImageUrlAttribute() {
        return Helper::getProfilePath($this->foto_anggota);
    }

    public function getCreatedAtAttribute()
	{
		return Carbon::parse($this->attributes['created_at'])
			->addHours(7)
            ->translatedFormat('Y-m-d H:i:s');
	}
}