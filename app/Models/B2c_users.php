<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class B2c_users extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'b2c_users';

    protected $primaryKey = 'id_user';


    public static $registerValidation = [
        'email'     => 'required|email|unique:b2c_users',
        'password'  => 'required|min:5',
        'fullname'  => 'required|min:2',
        'no_telp'   => 'required|min:10',
    ];

    public static $loginValidation = [
        'email' => 'required|email',
        'password' => 'required'
    ];

    public static $validateUser = [
        'id_user' => 'required',
        'fullname'  => 'required|min:2',
        'no_telp'   => 'required|min:10',
    ];

    public static $validateImage = [
        'image' => 'required|image',
        'id_user' => 'required',
    ];

    public static $validatePassword = [
        'id_user'       => 'required|min:20',
        'old_password'  => 'bail|required|min:5',
        'new_password'  => 'bail|required|min:5'
    ];

    protected $fillable = [
        'id_user','email', 'password','full_name','no_telp','pin',
    ];


    public $timestamps = false;

    public $incrementing = false;

    protected $hidden = [
        'password','created_at'
    ];
}
