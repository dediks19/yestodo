<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class B2c_group_members extends Model
{
	protected $table = 'b2c_group_members';

	protected $primaryKey = 'id_member';

	public $timestamps = false;

	public $incrementing = false;

	protected $fillable = [
		'id_member','fk_group','fk_user','status',
	];

	public static $validateID = [
		'id_user'=>'required|min:20',
		'id_member'=>'required|min:20',
	];

	public static $validatePIN = [
		'id_user'=> 'required',
	];

	public static $validationAdd = [
		'id_group' 	=> 'required|min:20',
		'id_user' 	=> 'required|min:20',
		'members' 	=> 'required',
	];

	public static $validationEdit = [
		'id_user' 	=> 'required',
	];

	public static $validateGet = [
		'id_user' 	=> 'required',
		'id_group' 	=> 'required'
	];
}