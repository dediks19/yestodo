<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class B2c_friends extends Model
{
	protected $table = 'b2c_friends';

	protected $primaryKey = 'id_friend';

	protected $fillable = [
		'id_friend','fk_user ','fk_user_friend','friend_status',
	];

	public static $validateID = [
		'id_user' => 'required|min:20',
	];

	public static $validatePIN = [
		'pin'=> 'required|min:8',
		'id_user'=> 'required',
	];

	public static $validateAddFriend = [
		'id_user' => 'required',
		'fk_user' => 'required',
	];

	public static $validateFriendID = [
		'id_friend' => 'required|min:20',
	];

	public $timestamps = false;
}