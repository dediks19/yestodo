<?php

namespace App\Helper;
use Illuminate\Support\Facades\App;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class Helper {

  public function ValidationCheck($request, $class) {
    
    $validator = Validator::make($request, $class);
      if ($validator->fails()) {
        http_response_code(400);
        echo json_encode([
          'error_status' => true,
          'code_status' => 400,
          'message'=>$validator->errors()->first(),
          'data' => ''
        ]);die();
      }

  }

  public function datetimeFormat($date, $time){
      $datetime = Carbon::createFromFormat('d-m-Y H:i',$date." ".$time)->format('Y-m-d H:i:s');
      return $datetime;
  }

  public function dateFormat($date){
    $dates = Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');
    return $dates;
  }

  public function getProfilePath($path){
    return "https://yestodo.co.id/api/images/profile/".$path;
  }

  public function getFriendID(){
    return DB::select("SELECT createB2C('friends') as id")[0]->id;
  }

  public function getUserID()
  {
    return DB::select("SELECT createB2C('users') as id")[0]->id;
  }

  public function getGroupID()
  {
    return DB::select("SELECT createB2C('groups') as id")[0]->id;
  }

  public function getGroupMemberID()
  {
    return DB::select("SELECT createB2C('members') as id")[0]->id;
  }

  public function getTaskID()
  {
    return DB::select("SELECT createB2C('task') as id")[0]->id;
  }

  public function getTaskMemberID()
  {
    return DB::select("SELECT createB2C('taskmember') as id")[0]->id;
  }

  public function getUserActivitesID()
  {
    return DB::select("SELECT createB2C('activity') as id")[0]->id;
  }

  public function getCommentID()
  {
    return DB::select("SELECT createB2C('comment') as id")[0]->id;
  }

  public function getProgressID()
  {
    return DB::select("SELECT createB2C('progress') as id")[0]->id;
  }

  public function getReportID(){
    return DB::select("SELECT createID('report') as id")[0]->id;
  }
}
