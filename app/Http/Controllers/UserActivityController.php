<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\B2c_users;
use App\Models\B2c_user_activities;
use DB;
use App\Helper\Helper;

class UserActivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    { }

    public function index(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_user_activities::$validateID);

        $history = B2c_user_activities::where('fk_task',$request->id_task)
                        ->select('id_activity','us.full_name as fullname','b2c_user_activities.created_at as created_at','att.describe as describe','fk_actitype as status')
                        ->leftJoin('b2c_activity_types as att','fk_actitype','=','att.id_actitype')
                        ->leftJoin('b2c_users as us','fk_user','=','us.id_user')
                        ->get();
        return response()->json(['error_status' => 'false',
                                'code_status'   => 200,
                                'message'       => 'succesfully',
                                'data'          => $history
                            ]);
    }
}