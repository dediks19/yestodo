<?php

namespace App\Http\Controllers;
use App\Models\B2c_groups;
use App\Models\B2c_group_members;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use App\Helper\Helper;
use File;
use Storage;

class GroupController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    	{ }

    public function index(Request $request)
    {
        if ($request->id_group) {
                Helper::ValidationCheck($request->all(), B2c_groups::$validateIDGroup);
                $group      = B2c_groups::where('id_group', $request->id_group)
                                ->where('active', 'y')
                                ->first();
                $members    = DB::table('b2c_group_members as gm')
                                ->select('gm.id_member','us.id_user','us.full_name','us.image','gm.status')
                                ->leftJoin('b2c_users as us', 'gm.fk_user','=','us.id_user')
                                ->where('fk_group', $group->id_group)
                                ->get();
                return response()->json(['id_group'     =>$group->id_group,
                                        'name'          =>$group->name,
                                        'describe'      =>$group->describe,
                                        'image'         =>$group->image,
                                        'created_at'    =>$group->created_at,
                                        'members'       =>$members]);
            }else{
                Helper::ValidationCheck($request->all(), B2c_groups::$validateID);
                $data = DB::table('b2c_groups')
                                    ->select('b2c_groups.id_group','b2c_groups.name','b2c_groups.image', DB::raw('count_membergroup(b2c_groups.id_group) as members'))
                                    ->leftJoin('b2c_group_members', 'b2c_group_members.fk_group', '=', 'b2c_groups.id_group')
                                    ->where('b2c_group_members.fk_user', $request->id_user)
                                    ->where('b2c_groups.name','LIKE', '%'. $request->q .'%')
                                    ->where('b2c_groups.active', 'y')
                                    ->simplePaginate(10);
                return response()->json($data);
        }
    	
    }

    public function create_group(Request $request)
    {
    	Helper::ValidationCheck($request->all(), B2c_groups::$validateID);
    	DB::beginTransaction();
    	$id_group = Helper::getGroupID();
    	$data = new B2c_groups;
    	$data->id_group 	= $id_group;
    	$data->name 		= $request->name;
    	$data->describe 	= $request->describe;
    	$data->created_by 	= $request->id_user;
    	$data->save();

    	$data1 = new B2c_group_members();
    	$data1->id_member		= Helper::getGroupMemberID();
    	$data1->fk_group 		= $id_group;
    	$data1->fk_user 		= $request->id_user;
    	$data1->status 			= 'admin';
    	$data1->save();

        foreach ($request->members as $key => $row) {
            $member = new B2c_group_members();
            $member->id_member      = Helper::getGroupMemberID();
            $member->fk_group        = $id_group;
            $member->fk_user         = $row['fk_user'];
            $member->status          = 'member';
            $member->save();
        }

    	if ($data && $data1) {
    		DB::commit();
    		return response()->json(['message'=>'success'], 201);
    	}else{
    		DB::rollback();
    		return response()->json(['message'=>'failed'], 400);
    	}
    }

    public function uploadImage(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_groups::$validateImage);
        $group = B2c_groups::where('id_group', $request->id_group)->first();
        $path   = storage_path('groups');
        $current_image_path = storage_path('groups') . '/' . $group->image;
        if (!empty($group->image)) {
            unlink($current_image_path);
        }
        $image_name = Str::random(20);
        if ($request->file('image')->move(storage_path('groups'), $image_name)) {
            $update = DB::table('b2c_groups')
                        ->where('id_group', $request->id_group)
                        ->update(['image' => $image_name,]);
            return response()->json(['message' => 'Gambar berhasil diganti!']);
        }else{
            return response()->json(['message' => 'Gagal mengganti gambar!/nCoba beberapa saat lagi.']);
        }
        
    }

    public function groupEdit(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_groups::$validateEdit);
        $group = B2c_groups::findOrFail($request->id_group);
        $group->name        = $request->name;
        $group->describe    = $request->describe;
        $group->update();

        if ($group) {
            return response()->json(['message'=>'success']);
        }else{
            return response()->json(['message'=>'failed'], 400);
        }

    }

    public function deleteGroup(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_groups::$validateIDGroup);
        $group = B2c_groups::findOrFail($request->id_group);
        $group->active = 'n';
        $group->update();

        if ($group)
        {
            return response()->json(['message'=>'success']);
        }else
        {
            return response()->json(['message'=>'failed'], 400);
        }
    }

    public function get_avatar($image)
    {
        $avatar_path = storage_path('groups') . '/' . $image;
        if (file_exists($avatar_path)) {
              $file = file_get_contents($avatar_path);
              return response($file, 200)->header('Content-Type', 'image/jpeg');
            }
        $res['success'] = false;
        $res['message'] = "Avatar not found";
            
            return $res;
    }

    
}