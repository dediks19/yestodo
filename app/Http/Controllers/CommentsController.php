<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\B2c_task;
use App\Models\B2c_task_members;
use App\Models\B2c_user_activities;
use App\Models\B2c_comments;
use DB;
use App\Helper\Helper;

class CommentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    { }

   public function addComment(Request $request)
   {
    Helper::ValidationCheck($request->all(), B2c_comments::$validateAddComment);

    $comment = new B2c_comments();
    $comment->id_comment    = Helper::getCommentID();
    $comment->fk_user       = $request->id_user;
    $comment->fk_task       = $request->id_task;
    $comment->comment       = $request->comment;
    $comment->save();

    if ($comment) {
        return response()->json(['message' => 'created'], 201);
    }else{
        return response()->json(['message' => 'Terjadi kesalahan! Coba beberapa saat lagi ya.'], 400);
    }
   }

}