<?php

namespace App\Http\Controllers;
use App\Models\B2c_friends;
use App\Models\B2c_users;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use App\Helper\Helper;

class FriendController extends Controller
{
	public function __construct()
    { }

    public function index(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_friends::$validateID);

    	$data = DB::table('b2c_friends')
    			->selectRaw('b2c_friends.id_friend as id_friend,
                    b2c_friends.fk_user_friend as fk_user,
    				b2c_users.full_name as fullname,
    				b2c_users.pin as pin,
    				b2c_users.image as image,
    				b2c_users.email as email')
    			->leftJoin('b2c_users', 'b2c_friends.fk_user_friend', '=', 'b2c_users.id_user')
    			->where('b2c_friends.fk_user', $request->id_user)
    			->where('b2c_friends.friend_status', 'y')
                ->where('b2c_users.full_name', 'LIKE','%'.$request->q.'%')
    						->simplePaginate(10);
    	return response()->json($data);
    }


    public function searchPIN(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_friends::$validatePIN);
    	$data = B2c_users::where('pin', $request->pin)
                            ->where('active', 'y')->first();
    	if (isset($data)) {
            $is_friend = B2c_friends::where('fk_user_friend', $data->id_user)
                                        ->where('fk_user', $request->id_user)->first();
            if ($is_friend) {
                return response()->json(['message'=>'Sudah menjadi kontak anda!'], 400);
            }else{
                return response()->json($data);    
            }
            
        }else{
            return response()->json(['message'=>'PIN tidak terdaftar'], 400);
        }
    }

    public function addFriend(Request $request)
    {
        if ($request->id_friend) {
            $id_friend = $request->id_friend;
            $data = B2c_friends::findOrFail($id_friend);
            $data->friend_status = 'y';
            $data->save();
            $contact = new B2c_friends();
            $contact->id_friend        = Helper::getFriendID();
            $contact->fk_user          = $data->fk_user_friend;
            $contact->fk_user_friend   = $data->fk_user;
            $contact->friend_status    = 'y';
            $contact->save();

            return response()->json(['message'=>'Kini menjadi kontak anda.'], 201);
        }else{
            Helper::ValidationCheck($request->all(), B2c_friends::$validateAddFriend);
            $data = new B2c_friends();
            $data->id_friend        = Helper::getFriendID();
            $data->fk_user          = $request->id_user;
            $data->fk_user_friend   = $request->fk_user;
            $data->save();

            return response()->json(['message'=>'Permintaan dikirim.'], 201);
        }
    }

    public function getFriendAcc(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_friends::$validateID);

        $data = DB::table('b2c_friends')
                ->selectRaw('b2c_friends.id_friend as id_friend,
                    b2c_friends.fk_user_friend as fk_user,
                    b2c_users.full_name as fullname,
                    b2c_users.pin as pin,
                    b2c_users.image as image,
                    b2c_users.email as email')
                ->leftJoin('b2c_users', 'b2c_friends.fk_user', '=', 'b2c_users.id_user')
                ->where('b2c_friends.fk_user_friend', $request->id_user)
                ->where('b2c_friends.friend_status', 'n')
                            ->simplePaginate(10);
        return response()->json($data);
    }

    public function unfriend(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_friends::$validateFriendID);
        $friend     = B2c_friends::findOrFail($request->id_friend);
        $friend1    = B2c_friends::where('fk_user', $friend->fk_user_friend)
                                        ->where('fk_user_friend', $friend->fk_user)
                                        ->first();
        if ($friend && $friend1) {
            DB::beginTransaction();
            $friend->delete();
            $friend1->delete();

            if ($friend && $friend1) {
                DB::commit();
                return response()->json(['message'=>'success']);
            }else{
                DB::rollback();
                return response()->json(['message'=>'fail'], 400);
            }
            
        }else{
            return response()->json(['message'=>'fail'], 400);
        }
    }

}