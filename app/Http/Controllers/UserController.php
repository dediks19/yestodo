<?php

namespace App\Http\Controllers;
use App\Models\B2c_users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Str;
use App\Helper\Helper;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    { }
	
	
	public function getUser(Request $request)
	{
		$data = B2c_users::where('id_user', $request->id_user)->first();
        return response()->json([
            'id_user'   => $data->id_user,
            'fullname'  => $data->full_name,
            'email'     => $data->email,
            'no_telp'   => $data->no_telp,
            'image'     => $data->image,
            'pin'       => $data->pin,
        ]);
	}

    public function editProfile(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_users::$validateUser);
        $exe = DB::table('b2c_users')
                            ->where('id_user', $request->id_user)
                            ->update([
                                'full_name' => $request->fullname,
                                'no_telp'   => $request->no_telp
                            ]);
        return response()->json(['message'=> 'Profil telah diperbarui.']);
    }

    public function uploadImage(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_users::$validateImage);
        
        $user = B2c_users::where('id_user', $request->id_user)->first();
        $current_image_path = storage_path('users') . '/' . $user->image;
        if (!empty($user->image)) {
            unlink($current_image_path);
        }

        $image_name = Str::random(20);
        if ($request->file('image')->move(storage_path('users'), $image_name)) {
            $update = DB::table('b2c_users')
                        ->where('id_user', $request->id_user)
                        ->update(['image' => $image_name,]);
            return response()->json(['message' => 'Gambar berhasil diganti!']);
        }else{
            return response()->json(['message' => 'Gagal mengganti gambar!/nCoba beberapa saat lagi.']);
        }

    }

    public function get_avatar($image)
    {
        $avatar_path = storage_path('users') . '/' . $image;
        if (file_exists($avatar_path)) {
              $file = file_get_contents($avatar_path);
              return response($file, 200)->header('Content-Type', 'image/jpeg');
            }
        $res['success'] = false;
            $res['message'] = "Avatar not found";
            
            return $res;
    }

    public function change_password(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_users::$validatePassword);

        $user = B2c_users::findOrFail($request->id_user)->first();

        if (Hash::check($request->old_password, $user->password)) {
            $new_password   = Hash::make($request->new_password);
            $user->password = $new_password;
            $user->save();

            if ($user) {
                return response()->json(['message' => 'success'], 200);
            }else{
                return response()->json(['message' => 'failed'], 400);
            }
        }else{
            return response()->json(['message' => 'Kata sandi lama yang anda masukan salah!\nPastikan kata sandi anda benar.', ],400);
        }

    }
}
