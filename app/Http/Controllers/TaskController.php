<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\B2c_task;
use App\Models\B2c_task_members;
use App\Models\B2c_user_activities;
use App\Models\B2c_comments;
use App\Models\B2c_friends;
use DB;
use App\Helper\Helper;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    { }

    public function index(Request $request){
        if ($request->id_task) {
            Helper::ValidationCheck($request->all(), B2c_task::$validateDetail);

            $task = B2c_task::where('id_task', $request->id_task)
                                ->first();
            $members = B2c_task_members::where('fk_task', $request->id_task)
                                            ->select('b2c_users.image as image')
                                            ->leftJoin('b2c_users', 'b2c_task_members.fk_user', '=','b2c_users.id_user')
                                            ->get();
            $comment = B2c_comments::where('fk_task', $request->id_task)
                                            ->select('id_comment', 'comment','b2c_comments.created_at as created_at','b2c_users.id_user','b2c_users.full_name as fullname','b2c_users.image as image')
                                            ->leftJoin('b2c_users', 'b2c_comments.fk_user', '=', 'b2c_users.id_user')
                                            ->get();
            return response()->json([
                'id_task'   => $task->id_task,
                'title'     => $task->title,
                'describe'  => $task->describe,
                'created_at'=> $task->created_at,
                'deadline'  => $task->deadline,
                'status'    => $task->fk_last_activity,
                'members'   => $members,
                'comments'  => $comment,
            ]);


        }else{
            Helper::ValidationCheck($request->all(), B2c_task::$validateGroupID);

            $task = B2c_task::where('fk_group', $request->fk_group)
                                ->where('fk_last_activity', $request->filter)
                                ->where('archived', 'n')
                                ->select('id_task','title', 'describe','deadline',DB::raw('countComment(id_task) as total_comment'))
                                
                                    ->simplePaginate(10);

            return response()->json($task);
        }
    }

    public function createTask(Request $request){
        Helper::ValidationCheck($request->all(), B2c_task::$validateTask);

        DB::beginTransaction();
        $task = new B2c_task();

        $id_task            = Helper::getTaskID();

        $task->id_task      = $id_task;
        $task->fk_group     = $request->fk_group;
        $task->title        = $request->title;
        $task->describe     = $request->describe;
        $task->archived     = 'n';
        $task->deadline     = Helper::datetimeFormat($request->datelimit, $request->timelimit);
        $task->created_by   = $request->id_user;
        $task->save();

        foreach ($request->members as $key => $row) {
            Helper::ValidationCheck($request->all(), B2c_task_members::$validateTaskMember);
            $member = new B2c_task_members();
            $member->id_task_member     = Helper::getTaskMemberID();
            $member->fk_user            = $row['fk_user'];
            $member->fk_task            = $id_task;
            $member->save();
        }

        $action = new B2c_user_activities();
        $action->id_activity = Helper::getUserActivitesID();
        $action->fk_user = $request->id_user;
        $action->fk_task = $id_task;
        $action->fk_actitype = '1';
        $action->save();

        if ($task && $member && $action) {
            DB::commit();
            return response()->json(['message'=>'success'], 201);
        }else{
            DB::rollback();
            return response()->json(['message'=>'failed'], 400);
        }
    }

    public function deleteTask(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_task::$validateDetail);
        $task = B2c_task::findOrFail($request->id_task);
        $task->archived = 'y';
        $task->save();

        if ($task) {
            return response()->json(['message' => 'deleted']);
        }else{
            return response()->json(['message' => 'Terjadi kesalahan! Silakan coba beberapa saat lagi.'], 400);
        }
    }

    public function test(Request $request)
    {
        $datetime = Helper::datetimeFormat($request->datelimit, $request->timelimit);
        return response()->json(['message', $datetime]);
    }

    public function moveTask(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_task::$validateMove);
        DB::beginTransaction();
        $task = B2c_task::findOrFail($request->id_task);
        $activity = new B2c_user_activities();
        $activity->id_activity  = Helper::getUserActivitesID();
        $activity->fk_user      = $request->id_user;
        $activity->fk_task      = $request->id_task;
        $activity->fk_actitype  = $request->activity;
        $activity->save();

        $task->fk_last_activity = $request->activity;
        $task->save();

        if ($task && $activity) {
            DB::commit();
            return response()->json(['message' => 'success']);
        }else{
            DB::rollback();
            return response()->json(['message' => 'failed'], 400);
        }
    }

    public function getArchived(Request $request)
    {
        if ($request->id_task) {
            Helper::ValidationCheck($request->all(), B2c_task::$validateDetail);

            $task = B2c_task::where('id_task', $request->id_task)
                                ->first();
            $members = B2c_task_members::where('fk_task', $request->id_task)
                                            ->select('b2c_users.image as image')
                                            ->leftJoin('b2c_users', 'b2c_task_members.fk_user', '=','b2c_users.id_user')
                                            ->get();
            $comment = B2c_comments::where('fk_task', $request->id_task)
                                            ->select('id_comment', 'comment','b2c_comments.created_at as created_at','b2c_users.id_user','b2c_users.full_name as fullname','b2c_users.image as image')
                                            ->leftJoin('b2c_users', 'b2c_comments.fk_user', '=', 'b2c_users.id_user')
                                            ->get();
            return response()->json([
                'id_task'   => $task->id_task,
                'title'     => $task->title,
                'describe'  => $task->describe,
                'created_at'=> $task->created_at,
                'deadline'  => $task->deadline,
                'status'    => $task->fk_last_activity,
                'members'   => $members,
                'comments'  => $comment,
            ]);
        }else{
            Helper::ValidationCheck($request->all(), B2c_task::$validateGroupID);

            $task = B2c_task::where('fk_group', $request->fk_group)
                                ->where('archived', 'y')
                                ->select('id_task','title', 'describe','deadline',DB::raw('countComment(id_task) as total_comment'))
                                
                                    ->simplePaginate(10);

            return response()->json($task);
        }
        
    }

    public function getMemberFromTaskMember(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_task::$validateTaskAndMember);

        $task = B2c_friends::where('fk_user', $request->id_user)
                            ->select('us.id_user as id_user','us.full_name as fullname','us.image as image')
                            ->leftJoin('b2c_users as us','fk_user_friend','=','us.id_user')
                            ->where('friend_status','y')
                            ->whereNotIn('fk_user_friend', function($query) use($request){
                                $query->select('tm.fk_user')
                                ->from('b2c_task_members as tm')
                                ->where('tm.fk_task', $request->id_task);
                            })
                            ->orderBy('us.full_name','ASC')
                            ->get();
        return response()->json($task);
    }

    public function addMemberToTask(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_task::$validateInput);
        DB::beginTransaction();

        foreach ($request->members as $key => $value) {
        $input = new B2c_task_members();
        $input->id_task_member  = Helper::getTaskMemberID();
        $input->fk_user         = $value['id_member'];
        $input->fk_task         = $request->id_task;
        $input->save();
        }

        if ($input) {
            DB::commit();
            return response()->json(['status' => 'success',
                                    'status_code' => '201',
                                    'data' => ''
            ]);
        }else{
            DB::rollback();
             return response()->json(['status' => 'failed',
                                    'status_code' => '400',
                                    'data' => ''
            ]);
        }

    }
}