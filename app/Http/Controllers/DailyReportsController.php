<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Daily_Reports;
use App\Models\Anggota_Sistem;
use DB;
use App\Helper\Helper;
use Illuminate\Support\Carbon;

class DailyReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    {$this->middleware("login"); }

    public function index(Request $request){
        if (isset($request->fk_user)) {
            Helper::ValidationCheck($request->all(), Daily_Reports::$validateFK);
            
            $report = Daily_Reports::where('to', $request->id_user)
                                    ->where('from', $request->fk_user);
            $user = Anggota_Sistem::where('id_anggota',$request->fk_user)->first();
            //DB::enableQueryLog();

            //filter with dates
            if (!empty($request->date_from && $request->date_to)) {
                $from   = Helper::dateFormat($request->date_from);
                $to     = Helper::dateFormat($request->date_to);
                $report->whereDate('created_at','>=', $from);
                $report->whereDate('created_at','<=', $to);
            }else{
                //no filter, default - 3 days
                $now   = Carbon::now()->format('Y-m-d');
                $to     = Carbon::now()->subDays(3)->format('Y-m-d');
                $report->whereDate('created_at','>=', $to);
                $report->whereDate('created_at','<=', $now);
            }
            $report = $report->get();

            if ($report && $user) {
                return response()->json([
                    'error_status'  => false,
                    'status_code'   => 200,
                    'message'       => 'found',
                    'data'          => [
                            'fullname'  => $user->nama_anggota,
                            'image'     => Helper::getProfilePath($user->foto_anggota),
                            'reports'   => $report
                        ]
                ]);
            }else{
                return response()->json([
                    'error_status'  => true,
                    'status_code'   => 400,
                    'message'       => 'cant fetch data',
                    'data'          => ''
                ],400);
            }

        }else{
            Helper::ValidationCheck($request->all(), Daily_Reports::$validateID);
            $report = Daily_Reports::groupBy('as.id_anggota')
                                        ->where('to', $request->id_user)
                                        ->select('as.id_anggota as id_user','as.nama_anggota as fullname','as.foto_anggota')
                                        ->leftJoin('anggota_sistem as as','daily_reports.from','=','as.id_anggota')
                                        ->get()
                                        ->append('image_url');
            if ($report) {
                return response()->json([
                    'error_status'  => false,
                    'status_code'   => 200,
                    'message'       => 'found',
                    'data'          => $report                
                ]);
            }else{
                return response()->json([
                    'error_status'  => true,
                    'status_code'   => 400,
                    'message'       => 'cant fetch data',
                    'data'          => ''
                ],400);
            }
        }
    }

    public function insertReport(Request $request){
        Helper::ValidationCheck($request->all(), Daily_Reports::$validateInput);

        DB::beginTransaction();
        if (empty($request->to)) {
            return response()->json([
                'error_status'  => true,
                'status_code'   => 400,
                'message'       => 'Recipient is required',
                'data'          => ''
            ], 400);
        }else{
            foreach ($request->to as $key => $value) {
                if (empty($value['fk_user'])) {
                    $report = false;
                }else{
                    $report = new Daily_Reports();
                    $report->id_report  = Helper::getReportID();
                    $report->message    = $request->message;
                    $report->from       = $request->id_user;
                    $report->to         = $value['fk_user'];
                    $report->save();
                }
                $statement[] = $report;
            }

            if (in_array(false, $statement)) {
                DB::rollback();
                return response()->json([
                    'error_status'  => true,
                    'status_code'   => 400,
                    'message'       => 'failed while insert data',
                    'data'          => ''
                ], 400);
            }else{
                DB::commit();
                return response()->json([
                    'error_status'  => false,
                    'status_code'   => 201,
                    'message'       => 'insert data succesfully',
                    'data'          => ''
                ],201);
            }
        }
    }

    public function getUserList(Request $request){
        Helper::ValidationCheck($request->all(), Anggota_Sistem::$validateID);

        $user = Anggota_Sistem::where('id_anggota', $request->id_user)
                                    ->leftJoin('jabatan','kd_jabatan','=','id_jabatan')
                                    ->first();
        
        DB::enableQueryLog();
        $users = Anggota_Sistem::where('anggota_sistem.kd_perusahaan', $user->kd_perusahaan)
                                ->where('level', '<', $user->level)
                                ->leftJoin('divisi','kd_divisi','=','id_divisi')
                                ->leftJoin('jabatan','kd_jabatan','=','id_jabatan')
                                ->select('id_anggota as id_user','nama_anggota as fullname','id_jabatan','nama_jabatan','id_divisi','nama_divisi','foto_anggota');
        if (!empty($request->q)) {
            $users->where('nama_anggota','LIKE',"%".$request->q."%");
        }

        $users = $users->get()->append('image_url');
        //var_dump(DB::getQueryLog());die();
        if ($users) {
            return response()->json([
                'error_status'  => false,
                'code_status'   => 200,
                'message'       => 'found',
                'data'          => $users
            ]);
        }else{
            return response()->json([
                'error_status'  => false,
                'code_status'   => 400,
                'message'       => 'failed fetch data',
                'data'          => ''
            ],400);
        }
    }

    public function getHistory(Request $request){
        if (isset($request->fk_user)) {
            Helper::ValidationCheck($request->all(), Daily_Reports::$validateFK);
            
            $report = Daily_Reports::where('from', $request->id_user)
                                    ->where('to', $request->fk_user);
            $user = Anggota_Sistem::where('id_anggota',$request->fk_user)->first();
            //DB::enableQueryLog();

            //filter with dates
            if (!empty($request->date_from && $request->date_to)) {
                $from   = Helper::dateFormat($request->date_from);
                $to     = Helper::dateFormat($request->date_to);
                $report->whereDate('created_at','>=', $from);
                $report->whereDate('created_at','<=', $to);
            }else{
                //no filter, default - 3 days
                $now   = Carbon::now()->format('Y-m-d');
                $to     = Carbon::now()->subDays(3)->format('Y-m-d');
                $report->whereDate('created_at','>=', $to);
                $report->whereDate('created_at','<=', $now);
            }
            $report = $report->get();

            if ($report && $user) {
                return response()->json([
                    'error_status'  => false,
                    'status_code'   => 200,
                    'message'       => 'found',
                    'data'          => [
                            'fullname'  => $user->nama_anggota,
                            'image'     => Helper::getProfilePath($user->foto_anggota),
                            'reports'   => $report
                        ]
                ]);
            }else{
                return response()->json([
                    'error_status'  => true,
                    'status_code'   => 400,
                    'message'       => 'cant fetch data',
                    'data'          => ''
                ],400);
            }

        }else{   
            Helper::ValidationCheck($request->all(), Daily_Reports::$validateID);
                $report = Daily_Reports::groupBy('as.id_anggota')
                                            ->where('from', $request->id_user)
                                            ->select('as.id_anggota as id_user','as.nama_anggota as fullname','as.foto_anggota')
                                            ->leftJoin('anggota_sistem as as','daily_reports.to','=','as.id_anggota')
                                            ->get()
                                            ->append('image_url');
                if ($report) {
                    return response()->json([
                        'error_status'  => false,
                        'status_code'   => 200,
                        'message'       => 'found',
                        'data'          => $report                
                    ]);
                }else{
                    return response()->json([
                        'error_status'  => true,
                        'status_code'   => 400,
                        'message'       => 'cant fetch data',
                        'data'          => ''
                    ],400);
                }
        }
    }
}