<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\B2c_users;
use DB;
use App\Helper\Helper;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    { }
	
    public function login(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_users::$loginValidation);
        $user = B2c_users::where('email', $request->input('email'))->first();
        if (isset($user->id_user)) {
            if(Hash::check($request->input('password'), $user->password)){
            return response()->json(['id_user'    => $user->id_user,
                                      'fullname'  => $user ->full_name,
                                      'image'     => $user ->image,
                                      'pin'       => $user->pin,
                                      'email'     => $user->email]);
          }else{
            return response()->json(['message' => 'Password yang anda masukan salah!'],401);
          }
        }else{
          return response()->json(['message' => 'Tidak ada akun terkait!'],401);
        }
        
   }

   public function register(Request $request){
    // $id_user = DB::select("SELECT createB2C('users') as id")[0]->id;
    $pin = $this->generatePIN(DB::select('SELECT uuid() as pin')[0]->pin) ;

    Helper::ValidationCheck($request->all(), B2c_users::$registerValidation);
    $password = Hash::make($request->input('password'));
    $user     = new B2c_users();
    $user->id_user    = Helper::getUserID();
    $user->full_name  = $request->fullname;
    $user->email      = $request->email;
    $user->no_telp    = $request->no_telp;
    $user->password   = $password;
    $user->pin        = $pin;
    $user->save();

    /*
    $users = B2c_users::create([
      'id_user' => $id_user,
      'full_name' => $request->input('fullname'),
      'email' => $request->input('email'),
      'no_telp' => $request->input('no_telp'),
      'password' => $password,
      'pin'  => $pin,
    ]); 
    */

    return response()->json(['message' => 'Berhasil membuat akun baru!'], 201);
   }

  function generatePIN($uuid) {
    $array = explode("-",$uuid);
    return $array[0];
  }
}