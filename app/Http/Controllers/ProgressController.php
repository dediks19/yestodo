<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\B2c_task;
use App\Models\B2c_task_members;
use App\Models\B2c_user_activities;
use App\Models\B2c_comments;
use App\Models\B2c_progress;
use DB;
use App\Helper\Helper;

class ProgressController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    { }

    public function getTaskMember(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_task_members::$validateTask);

        $task_members = B2c_task_members::where('fk_task', $request->id_task)
                                                ->select('id_task_member', 'us.full_name as fullname','us.image as image')
                                                ->leftJoin('b2c_users as us', 'b2c_task_members.fk_user', '=', 'us.id_user')
                                                ->get();

        return response()->json($task_members);

    }

    public function createProgress(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_progress::$validateCreate);

        $progress = new B2c_progress();
        $progress->id_progress  = Helper::getProgressID();
        $progress->fk_user      = $request->id_user;
        $progress->fk_task_member = $request->id_task_member;
        $progress->texts         = $request->texts;
        $progress->save();

        if ($progress) {
            return response()->json(['message'=>'created'], 201);
        }else{
            return response()->json(['message'=>'Terjadi kesalahan! Coba beberapa saat lagi.'], 400);
        }
    }

    public function index(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_progress::$validateID);

        $progress = B2c_progress::where('fk_task_member', $request->id_task_member)
                            ->select('id_progress','full_name','image','texts','b2c_progress.created_at as created_at')
                            ->leftJoin('b2c_users as us', 'fk_user' ,'=' ,'id_user')
                            ->orderBy('b2c_progress.created_at', 'ASC')
                            ->get();
        return response()->json($progress);
    }
}