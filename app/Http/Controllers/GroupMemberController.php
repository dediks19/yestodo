<?php 
namespace App\Http\Controllers;
use App\Models\B2c_groups;
use App\Models\B2c_group_members;
use App\Models\B2c_users;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use App\Helper\Helper;

class GroupMemberController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	 public function __construct()
    	{ }

    public function index(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_group_members::$validateGet);
        $members = DB::table('b2c_friends as ff')
                    ->select('us.id_user','us.full_name','us.image')
                    ->leftJoin('b2c_users as us', 'ff.fk_user_friend', '=', 'us.id_user')
                    ->where('ff.fk_user', $request->id_user)
                    ->where('ff.friend_status', 'y')
                    ->whereNotIn('ff.fk_user_friend', function($query) use($request) {
                        $query->select('fk_user')
                                ->from('b2c_group_members')
                                ->where('fk_group', $request->id_group);
                    })->get();
        if ($members) {
            return response()->json($members);
        }else{
            return response()->json(['message'=>'error'], 400);
        }

    }

    public function addMember(Request $request)
    {
    	Helper::ValidationCheck($request->all(), B2c_group_members::$validationAdd);
        $admin = DB::table('b2c_group_members')
                        ->where('fk_group', $request->id_group)
                        ->where('fk_user', $request->id_user)
                        ->first();
        if ($admin->status == 'admin') {
            $id_group = $request->id_group;
            foreach ($request->members as $key => $row) {
                $member = new B2c_group_members();
                $member->id_member      = Helper::getGroupMemberID();
                $member->fk_group       = $id_group;
                $member->fk_user        = $row['fk_user'];
                $member->status         = 'member';
                $member->save();
            }
            return response()->json(['message'=>'success'],201);
        }else{
            return response()->json(['message'=>'cannot access', 401]);
        }
        
    }

    public function editMember(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_group_members::$validationEdit);
        $user = DB::table('b2c_group_members')
                    ->where('fk_group', $request->id_group)
                    ->where('fk_user', $request->id_user)
                    ->first();
        if ($user->status == 'admin')
        {
            if ($request->status)
            {
                switch ($request->status)
                {
                    case 'manager':
                    $member = B2c_group_members::findOrFail($request->id_member);
                    $member->status = 'manager';
                    $member->update();

                    if ($member) {
                        return response()->json(['message'=>'set as manager success']);
                    }else{
                        return response()->json(['message'=>'set as manager failed'], 400);
                    }
                    break;
                
                    case 'member':
                        $member = B2c_group_members::findOrFail($request->id_member);
                        $member->status = 'member';
                        $member->update();
                        if ($member) {    
                            return response()->json(['message'=>'set as member success']);
                        }else{
                            return response()->json(['message'=>'set as member failed'], 400);
                        }
                        break;

                    default:
                    return response()->json(['message'=>'you dont belong here'], 401);
                        break;
                }
            }
        }
    }

    public function deleteMember(Request $request)
    {
        Helper::ValidationCheck($request->all(), B2c_group_members::$validateID);
        $group = B2c_group_members::findOrFail($request->id_member);
        $group->delete();

        if ($group) {
            return response()->json(['message'=>'success']);
        }else{
            return response()->json(['message'=>'not member in this group'], 400);
        }
        
    }
}