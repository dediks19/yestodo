<?php

namespace App\Http\Middleware;

use Closure;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($request->header('token'))) {
            $word = "ags12345";
            if ($request->header('token') == $word) {
                return $next($request);
            }else{
                return response()->json([
                    'error_status'  => true,
                    'status_code'   => 401,
                    'message'       => 'Unauthorized',
                    'data'          => ''
                ],401);
            }
        }else{
            return response()->json([
                'error_status'  => true,
                'status_code'   => 403,
                'message'       => 'Unauthorized',
                'data'          => ''
            ],401);
        }
    }
}
