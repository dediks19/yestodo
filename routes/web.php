<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/* Profile */
$router->get('/user/', 'UserController@getUser');
$router->put('/user', 'UserController@editProfile');
$router->post('/user', 'UserController@uploadImage');
$router->get('/user/image/{image}', 'UserController@get_avatar');

/* Authentication */
$router->post('/register', 'AuthController@register');
$router->get('/login/', 'AuthController@login');
$router->put('/password', 'UserController@change_password');

/* Contact */
$router->get('/contact/all/', 'FriendController@index');
$router->get('/contact/pin/', 'FriendController@searchPIN');
$router->post('/contact/add', 'FriendController@addFriend');
$router->get('/contact/acc/', 'FriendController@getFriendAcc');
$router->delete('/contact', 'FriendController@unfriend');

/* Groups */
$router->get('/group', 'GroupController@index');
$router->post('/group', 'GroupController@create_group');
$router->post('/group/image', 'GroupController@uploadImage');
$router->put('/group', 'GroupController@groupEdit');
$router->get('/group/image/{image}', 'GroupController@get_avatar');


/* Members */
$router->get('/member', 'GroupMemberController@index');
$router->put('/member/manage', 'GroupMemberController@editMember');
$router->post('/member', 'GroupMemberController@addMember');
$router->delete('/member', 'GroupMemberController@deleteMember');

/* Task */
$router->get('/task', 'TaskController@index');
$router->post('/task', 'TaskController@createTask');
$router->post('/test', 'TaskController@test');
$router->delete('/task', 'TaskController@deleteTask');
$router->put('/task', 'TaskController@moveTask');
$router->get('/archived', 'TaskController@getArchived');
$router->get('/task_member', 'TaskController@getMemberFromTaskMember');
$router->post('/task_member', 'TaskController@addMemberToTask');

/* Comments */
$router->post('/comment', 'CommentsController@addComment');

/* Progress */
$router->post('/progress', 'ProgressController@createProgress');
$router->get('/progress', 'ProgressController@index');
$router->get('/task_member_progress', 'ProgressController@getTaskMember');

/* History */
$router->get('/history', 'UserActivityController@index');


/* -----YES TO DO B2B----- */
/* Daily Report */
$router->post('/report', 'DailyReportsController@insertReport');
$router->get('/report', 'DailyReportsController@index');
$router->get('/users', 'DailyReportsController@getUserList');
$router->get('/report/history', 'DailyReportsController@getHistory');
